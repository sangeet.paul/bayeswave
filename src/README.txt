I added this line to the file.

-- BASIC USE -- 

To check out the SVN:
svn checkout  https://svn.ligo.caltech.edu/svn/bayeswave

To add a new file (or directory):
svn add README.txt
svn commit -m "Fist commit"

To make the first import, copy the source code into the "trunk" directory.  Then, 
move into the trunk directory, and run
svn add CODEDIRECTORY
svn commit -m "Tyson's first commit"

The "add" command tells the SVN to start tracking that particular file, and only
gets run once per new file.  The commit command pushes your changes to the remote server.
Run this command every time you have some changes that you want to keep.

To pull changes from the server to your local copy, run 
svn update

A good habit is to run "svn update" every time you sit down to start work, and 
run "svn commit" every time you are finished for the day.

- ADVANCED -

The "branches" and "tags" directories are not special, but are just place holders
in case we want to merge or make "releases" at some point.  

svn copy /trunk/src /branches/crazy_prototype

Then, if we want these things back in the trunk, you run
svn merge /branches/crazy_protoype /trunk/src

