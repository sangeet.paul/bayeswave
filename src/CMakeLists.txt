# -- C library --------------

# library version | the rules for shared library version are complicated,
#                   the only very important thing is that if the ABI
#                   changes in a backwards-incompatible way, you should
#                   increment the MAJOR version number (and probably reset
#                   the others)
set(BAYESWAVE_VERSION_MAJOR 1)
set(BAYESWAVE_VERSION_MINOR 0)
set(BAYESWAVE_VERSION_PATCH 3)
set(BAYESWAVE_VERSION_STRING
    ${BAYESWAVE_VERSION_MAJOR}.${BAYESWAVE_VERSION_MINOR}.${BAYESWAVE_VERSION_PATCH})

# find libraries
pkg_check_modules(GSL REQUIRED "gsl")
find_library(LIBGSL "gsl" PATHS ${GSL_LIBRARY_DIRS})
message(STATUS "  Found libgsl in ${LIBGSL}")
pkg_check_modules(FFTW3 REQUIRED "fftw3")
find_library(LIBFFTW3 "fftw3" PATHS ${FFTW3_LIBRARY_DIRS})
message(STATUS "  Found libfftw3 in ${LIBFFTW3}")
pkg_check_modules(FFTW3F REQUIRED "fftw3f")
find_library(LIBFFTW3F "fftw3f" PATHS ${FFTW3F_LIBRARY_DIRS})
message(STATUS "  Found libfftw3f in ${LIBFFTW3F}")
pkg_check_modules(LAL REQUIRED "lal")
find_library(LIBLAL "lal" PATHS ${LAL_LIBRARY_DIRS})
message(STATUS "  Found liblal in ${LIBLAL}")
pkg_check_modules(LALSUPPORT REQUIRED "lalsupport")
find_library(LIBLALSUPPORT "lalsupport" PATHS ${LALSUPPORT_LIBRARY_DIRS})
message(STATUS "  Found liblalsupport in ${LIBLALSUPPORT}")
pkg_check_modules(LALFRAME REQUIRED "lalframe")
find_library(LIBLALFRAME "lalframe" PATHS ${LALFRAME_LIBRARY_DIRS})
message(STATUS "  Found liblalframe in ${LIBLALFRAME}")
pkg_check_modules(LALINFERENCE REQUIRED "lalinference")
find_library(LIBLALINFERENCE "lalinference" PATHS ${LALINFERENCE_LIBRARY_DIRS})
message(STATUS "  Found liblalinference in ${LIBLALINFERENCE}")
pkg_check_modules(LALSIMULATION REQUIRED "lalsimulation")
find_library(LIBLALSIMULATION "lalsimulation" PATHS ${LALSIMULATION_LIBRARY_DIRS})
message(STATUS "  Found liblalsimulation in ${LIBLALSIMULATION}")

# -- VCS info --------

execute_process(COMMAND
    "${GIT_EXECUTABLE}" config --get remote.origin.url
    WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
    OUTPUT_VARIABLE GIT_URL
    ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
message(STATUS "GIT_URL is ${GIT_URL}")
execute_process(COMMAND
    "${GIT_EXECUTABLE}" describe --dirty --always --tags
    WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
    OUTPUT_VARIABLE GIT_VER
    ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
message(STATUS "GIT_VER is ${GIT_VER}")
execute_process(COMMAND
    "${GIT_EXECUTABLE}" log -1 --format=%H
    WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
    OUTPUT_VARIABLE GIT_HASH
    ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
message(STATUS "GIT_HASH is ${GIT_HASH}")
execute_process(COMMAND
    "${GIT_EXECUTABLE}" log -1 --format=%ae
    WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
    OUTPUT_VARIABLE GIT_AUTHOR
    ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
message(STATUS "GIT_AUTHOR is ${GIT_AUTHOR}")
execute_process(COMMAND
    "${GIT_EXECUTABLE}" log -1 --format=%ad
    WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
    OUTPUT_VARIABLE GIT_DATE
    ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
message(STATUS "GIT_DATE is ${GIT_DATE}")
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/version.h.in
    ${CMAKE_CURRENT_SOURCE_DIR}/version.h @ONLY)

# define library
set(
    BAYESWAVE_HEADERS
    version.h
    BayesLine.h
    BayesWave.h
    BayesWaveEvidence.h
    BayesWaveIO.h
    BayesWaveLikelihood.h
    BayesWaveMCMC.h
    BayesWaveMath.h
    BayesWaveModel.h
    BayesWavePrior.h
    BayesWaveProposal.h
    BayesWaveWavelet.h
)
add_library(
    libbayeswave SHARED
    BayesLine.c
    BayesWaveIO.c
    BayesWaveMath.c
    BayesWaveModel.c
    BayesWaveWavelet.c
    BayesWavePrior.c
    BayesWaveLikelihood.c
    BayesWaveProposal.c
    BayesWaveEvidence.c
    BayesWaveMCMC.c
)
target_include_directories(libbayeswave PUBLIC
    ${GSL_INCLUDE_DIRS}
    ${FFTW3_INCLUDE_DIRS}
    ${FFTW3F_INCLUDE_DIRS}
    ${LAL_INCLUDE_DIRS}
    ${LALSUPPORT_INCLUDE_DIRS}
    ${LALFRAME_INCLUDE_DIRS}
    ${LALINFERENCE_INCLUDE_DIRS}
    ${LALSIMULATION_INCLUDE_DIRS}
    )
target_link_libraries(libbayeswave
    m
    ${LIBGSL}
    ${LIBFFTW3}
    ${LIBFFTW3F}
    ${LIBLAL}
    ${LIBLALSUPPORT}
    ${LIBLALFRAME}
    ${LIBLALINFERENCE}
    ${LIBLALSIMULATION}
    )
set_target_properties(
    libbayeswave PROPERTIES
    OUTPUT_NAME bayeswave
    VERSION ${BAYESWAVE_VERSION_STRING}
    SOVERSION ${BAYESWAVE_VERSION_MAJOR}
    PUBLIC_HEADER "${BAYESWAVE_HEADERS}"
)
set_property(TARGET libbayeswave PROPERTY C_STANDARD 11)

# install
install(
    TARGETS libbayeswave
    RUNTIME DESTINATION ${CMAKE_INSTALL_LIBDIR}
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
    PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
)

# -- executables --------
add_executable(BayesWave BayesWave.c)
target_link_libraries(BayesWave libbayeswave)
set_property(TARGET BayesWave PROPERTY C_STANDARD 11)

add_executable(BayesWavePost BayesWavePost.c)
target_link_libraries(BayesWavePost libbayeswave)
set_property(TARGET BayesWavePost PROPERTY C_STANDARD 11)

add_executable(BayesWaveCleanFrame BayesWaveCleanFrame.c)
target_link_libraries(BayesWaveCleanFrame libbayeswave)
set_property(TARGET BayesWaveCleanFrame PROPERTY C_STANDARD 11)

add_executable(BayesWaveToLALPSD BayesWaveToLALPSD.c)
target_link_libraries(BayesWaveToLALPSD libbayeswave)
set_property(TARGET BayesWaveToLALPSD PROPERTY C_STANDARD 11)

install(
    TARGETS
    BayesWave
    BayesWavePost
    BayesWaveCleanFrame
    BayesWaveToLALPSD
    DESTINATION ${CMAKE_INSTALL_BINDIR}
)

# -- pkgconfig --------------

# install pkgconfig file
configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME}.pc.in"
    "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.pc"
    @ONLY
)
install(
    FILES ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.pc
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/pkgconfig
)
