/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

/* ********************************************************************************** */
/*                                                                                    */
/*                                  LAL Dependencies                                  */
/*                                                                                    */
/* ********************************************************************************** */

#include <lal/LALInspiral.h>
#include <lal/LALCache.h>
#include <lal/LALFrStream.h>
#include <lal/TimeFreqFFT.h>
#include <lal/LALDetectors.h>
#include <lal/ResampleTimeSeries.h>
#include <lal/TimeSeries.h>
#include <lal/FrequencySeries.h>
#include <lal/Units.h>
#include <lal/Date.h>
#include <lal/StringInput.h>
#include <lal/VectorOps.h>
#include <lal/Random.h>
#include <lal/LALNoiseModels.h>
#include <lal/XLALError.h>
#include <lal/GenerateInspiral.h>
#include <lal/LIGOLwXMLRead.h>
#include <lal/LIGOLwXMLInspiralRead.h>
#include <lal/LALConstants.h>
#include <lal/SeqFactories.h>
#include <lal/DetectorSite.h>
#include <lal/GenerateInspiral.h>
#include <lal/GeneratePPNInspiral.h>
#include <lal/SimulateCoherentGW.h>
//#include <lal/Inject.h>
#include <lal/LIGOMetadataTables.h>
#include <lal/LIGOMetadataUtils.h>
#include <lal/LIGOMetadataInspiralUtils.h>
#include <lal/LIGOMetadataRingdownUtils.h>
#include <lal/LALInspiralBank.h>
#include <lal/FindChirp.h>
#include <lal/LALInspiralBank.h>
#include <lal/GenerateInspiral.h>
#include <lal/NRWaveInject.h>
#include <lal/GenerateInspRing.h>
#include <lal/LALError.h>

#include <lal/LALInference.h>
#include <lal/LALInferenceReadData.h>

/* ********************************************************************************** */
/*                                                                                    */
/*                            Globally defined parameters                             */
/*                                                                                    */
/* ********************************************************************************** */

#define RTPI 1.772453850905516       // sqrt(Pi)
#define RT2PI 2.5066282746310005024  // sqrt(2 Pi)
#define CLIGHT 2.99792458e8          // Speed of light (m/s)

#define NE 6 //Number of extrinsic parameters

//Tuning parameters for wavelet proximity proposal
//TODO: These should not be global!

#define UNIFORM 0.2    // fraction of proximity prior that is uniform
#define SPRD 4         // width of outer part of proximity proposal
#define HLW 1          // width of hollowed-out region

#define MAXSTRINGSIZE 1024

/* ********************************************************************************** */
/*                                                                                    */
/*                                  Data structures                                   */
/*                                                                                    */
/* ********************************************************************************** */
struct Wavelet
{
   int size; // number of wavelets
   int smax; // prior upper bound on number of wavelets
   int *index; // keeps track of which of the smax possible wavelets are active
   int dimension; // number of intrinsic parameters of each wavelet (5 of 6)

   //clustering prior
   int Ncluster; // DOESN'T WORK
   double cosy; // DOESN'T WORK
   double logp; // DOESN'T WORK

   double *templates; // fourier amplitudes evaluation of the wavelet (size N)
   double **intParams; // parameters of each wavelet (smax times dimension)

   char *name; // not used, meant to be "wavelet" or "chirplet"
};

struct Model
{
  int size; // total number of wavelets in all IFOs (glitch model) or polarization modes (signal model)
  int N; // number of data points (srate times Tobs)
  int Npol; // total number of polarization states we consider

  double **Snf; // PSD
  double **invSnf; // one over PSD at each frequency
  double **SnS; // spline part of the PSD
  double *SnGeo; // "geocenter PSD"

  double logL; // log of the likelihood
  double logLnorm; // log of the likelihood normalized (with the Sn part)
  double *detLogL; // log of the likelihood in each detector
  double *extParams; // extrinsic paramerers
  double **h; // reference waveform at one interferometer (NpolS times N)
  double **response; // response in each detector (NI times N)
    
  int chirpletFlag; // whether to use chirplets
  int NW; // number of intrinsic wavelet parameters

  struct Wavelet **signal; // structure that holds the signal wavelets (see appropriate structure definition)
  struct Wavelet **glitch; // structure that holds the glitch wavelets (see appropriate structure definition)

  struct Network *projection; // structure that holds the network projection (see appropriate structure definition)

  struct FisherMatrix *fisher; // structure that holds the fisher matrix (see appropriate structure definition)
  struct FisherMatrix *intfisher; // structure that holds the fisher matrix of the intrinsic parameters (see appropriate structure definition)

  struct Background *background; // structure that holds the stochastic background model (see appropriate structure definition)

  void (*wavelet_bandwidth)(double *, double *, double *); // function that points to the type of basis function used (wavelet or chirplet)
  void (*wavelet)(double *, double *, int, int, double); // function that points to the type of basis function used (wavelet or chirplet)

};

struct Network
{
  double **expPhase; // dummy parameters for the recursion relations
  double *deltaT; // dummy parameters for the recursion relations
  double *Fplus; // antenna pattern functions
  double *Fcross; // antenna pattern functions
  double *Fv1; // antenna pattern functions
  double *Fv2; // antenna pattern functions
  double *Fb; // antenna pattern functions
  double *Fl; // antenna pattern functions
  double *dtimes; //time delay between reference location and IFOs
};

struct Chain
{
  int mc;     // keeps track of total number of steps in the chain
  int NC;     // Number of parallel chains
  int burn;   // Number of burn in samples
  int mod0;   // Counter keeping track of noise model  REMOVE
  int mod1;   // Counter keeping track of glitch model
  int mod2;   // Counter keeping track of signal model
  int mod3;   // Counter keeping track of glitch+signal model

  int cycle;  // # of model updates per BurstRJMCMC iteration
  int count;  // # of BurstRJMCMC iterations
  int zcount; // # of logL samples for thermodynamic integration
  int mcount; // # of MCMC attempts
  int scount; // # of RJMCMC attempts per polarization mode
  int xcount; // # of extrinsic attempts
  int fcount; // # of intrinsic fisher attempts
  int pcount; // # of intrinsic phase attempts
  int ccount; // # of cluster proposal attempts
  int dcount; // # of density proposal attempts
  int ucount; // # of uniform proposal attempts
  int macc;   // # of MCMC successess
  int sacc;   // # of RJMCMC successes per polarization mode
  int xacc;   // # of extrinsic successes
  int facc;   // # of intrinsic fisher successes
  int pacc;   // # of intrinsic phase successes
  int cacc;   // # of cluster proposal successes
  int dacc;   // # of cluster proposal successes
  int uacc;   // # of uniform proposal successes

  int burnFlag;// flag telling proposals if we are in burn-in phase

  int *index; // keep track of which chain is at which temperature
  int *ptacc; // number of PTMCMC steps accepted
  int *ptprop; // number of PTMCMC proposals

  double *A; // adapt temperature

  gsl_rng *seed; 

  double modelRate;   //fraction of updates to signal model
  double rjmcmcRate;  //fraction of trans- vs. inter-dimensional moves
  double intPropRate; //fraction of prior vs. fisher intrinsic moves

  double *dT; // temperature difference
  double beta; // one over the temperature
  double tempMin; // minimum temperature
  double tempStep; // initial chain spacing
  double *temperature; // array that holds the chain temperatures
  double *avgLogLikelihood; // average log L per chain
  double *varLogLikelihood; // variance of logL per chain

  int nPoints; // how many likelihood samples to use to compute avgLogLikelihood and varLogLikelihood: 3*chain->count/4/chain->cycle
  double **logLchain;// likelihood samples used for thermodynamic integration


  FILE *runFile;
  FILE **intChainFile;
  FILE ***intWaveChainFile;
  FILE ***intGlitchChainFile;
  FILE ***lineChainFile;
  FILE ***splineChainFile;

};

struct Prior
{
  int smin; // minimum number of wavelets
  int smax; // maximum number of wavelets
  int *gmin; // minimum number of glitch wavelets (NOT USED)
  int *gmax; // maximum number of glitch wavelets (NOT USED)
  int omax; //REMOVE

  double Snmin; // minimum spline amplitude
  double Snmax; // maximum spline amplitude

  double TFV; // time-frequency volume
  double logTFV; // log of time-frequency volume

  double *bias; // DOESN'T WORK
  double **range; // prior range boundaries

  //exploratory background prior parameters, they are not currently used, should be revisited
    char bkg_name[1024];
    double bkg_df;
    double bkg_dQ;
    int bkg_nf;
    int bkg_nQ;
    double **bkg_dist;

  //cluster prior parameters
  double alpha; // DOESN'T WORK
  double beta; // DOESN'T WORK
  double gamma; // DOESN'T WORK

  //amplitude prior parameters
  double sSNRpeak; // peak of the amplitude prior per signal wavelet

  //glitch prior parameters
  double gSNRpeak; // peak of the amplitude prior per glitch wavelet

  //dimension prior parameters
  double Dtau; // REMOVE and from the cmd line output
  double *Nwavelet; //prior on the number of wavelets (smax)

  char path[100];

};

struct Data
{
  int N; // number of data points
  int NI; // number of IFOs
  int Npol; // total number of polarization states we consider
  int tsize; // time bins in the TF proposal
  int fsize; // frequency bins in the TF proposal
  int imin; // minimum frequency bin for inner product
  int imax; // maximum frequency bin for inner product
  int Dmax; // maximum number of wavelets
  int Dmin; // minimum number of wavelets

  int runPhase; // (0: cleaning, 1: not cleaning phase)
  int noiseSimFlag; // REMOVE
  int bayesLineFlag; // use BayesLine
  int stochasticFlag; // whether you use the stochastic model

  // type of run modes available (uses a combination of the models below)
  int fullModelFlag; 
  int noiseModelFlag;
  int glitchModelFlag;
  int signalModelFlag;
  int cleanModelFlag;
  int cleanOnlyFlag;

  // models available
  int cleanFlag;
  int glitchFlag;
  int signalFlag;
  int noiseFlag;
    
  int skyFixFlag; //fix the sky location to the given values

  int polarizationFlag; // use the elliptical polarization constraint
  int fixIntrinsicFlag; // fix intrinsic parameters
  int fixExtrinsicFlag; // fix extrinsic parameters
  int geocenterPSDFlag; // use the geocenter PSD fo the amplitude prior
  int clusterPriorFlag; // DOESN'T WORK
  int waveletPriorFlag; // use the prior on the number of wavelets
  int backgroundPriorFlag; // use the backgroung prior on the parameters of the glitch wavelets
  int amplitudePriorFlag; // use the SNR-based prior on the wavelet amplitude
  int amplitudeProposalFlag; // draw from the SNR-based prior on the wavelet amplitude as a proposal
  int signalAmplitudePriorFlag; // use the SNR-based prior on the wavelet amplitude for the signal model only
  int extrinsicAmplitudeFlag; // DOESN'T WORK, revisit
  int orientationProposalFlag; // proposal that updates psi and ecc
  int clusterProposalFlag; // propose wavelets close to existing wavelets (depends on SNR)
  int logClusterProposalFlag; // propose wavelets close to existing wavelets (depends on log SNR)
  int adaptTemperatureFlag; // adapt the PTMCMC temperature
  int splineEvidenceFlag; // use splines and MCMC for the thermodynamic integration
  int constantLogLFlag; // fix the likelihood to a constant
  int flatBayesLinePriorFlag; // options for when running on loud glitches, need to be revisited
  int loudGlitchFlag; // options for when running on loud glitches, need to be revisited
  int gnuplotFlag; // print 200 samples (waveforms and other diagnostics)
  int verboseFlag; // print every 100 iterations at stdout
  int checkpointFlag; // use periodic checkpointing
  int resumeFlag; // resume after checkpointing
  int searchFlag; //depricated, maximized logL mode
  int waveletStartFlag;
  int rjFlag; // whether you do RJ

  int nukeFlag; //tell BWP to remove chains/ and checkpoint/ directories

  int srate; // sampling rate
  
  int chirpletFlag; //use chirplets as a basis function 
  int NW; // number of intrinsic wavelet parameters
    
  double dt; // time resolution
  double df; // frequency resolution
  double fmin; // minimum frequency for inner products
  double fmax; // maximum frequency for inner products
  double Tobs; // segment length
  double Twin; // window length
  double Qmin; // minimum wavelet quality factor
  double Qmax; // maximum wavelet quality factor
  double gmst; // Greenwich time
  double logLc; // constant factor that can be applied to the likelihood
  double seglen;
  double trigtime;
  double starttime;
  double TwoDeltaToverN; // what it says
  
  double FIX_RA; // fixed right ascension value
  double FIX_DEC; //fixed declination value

  double TFtoProx; // ratio of TF to proximity proposals

  double **r; // residual
  double **s; // data h(t)
  double **rh; // depricated
  double *h; // model
  double *ORF; // overlap reduction function

  double logZglitch; // glitch evidence
  double logZnoise; // signal evidence
  double logZsignal; // noise evidence
  double logZfull; // full model evidence

  double varZglitch; // variation of the glitch evidence
  double varZnoise; // variation of the signal evidence
  double varZsignal; // variation of the noise evidence
  double varZfull; // variation of the full model evidence


  void (*signal_amplitude_proposal)(double *, double *, gsl_rng *, double, double **, double);
  void (*glitch_amplitude_proposal)(double *, double *, gsl_rng *, double, double **, double);

  double (*signal_amplitude_prior)   (double *, double *, double, double);
  double (*glitch_amplitude_prior)   (double *, double *, double, double);

  double (*intrinsic_likelihood)(int, int, int, struct Model*, struct Model*, struct Prior*, struct Chain*, struct Data*);
  double (*extrinsic_likelihood)(struct Network*, double *, double **, struct Wavelet **, double **, struct Data*, double, double);

  char runName[100];
  char outputDirectory[1000];
  char **channels;
  char **ifos;
  char **PolArray;
  char PolModes[10];

  ProcessParamsTable *commandLine;
  LALDetector **detector;
  LIGOTimeGPS epoch;
};

struct TimeFrequencyMap
{
  int N; // number of samples
  int nQ; // number of Q bins
  int nt; // number of t bins
  int nf; // number of f bins
  double ****pdf; // TF density
  double ****snr; // placeholder
  double **max;// max density 

};

struct FisherMatrix
{
  int N; // number of parameters
   double *sigma; // diagonal elements
   double *evalue; // eigenvalues
   double **evector; // eigenvectors
   double **matrix; // 2d NxN fisher matrix
   double *aamps; // jump sizes
   double **count; // deprecated
};

struct Background
{
  double logamp; // amplitude of the background
  double index; // spectral index
  double fref; // reference frequency
  double invfref; // one over the above
  double ***Cij;    // actually stores the inverse noise covariance matrix
  double *detCij;   // frequency-dependent determinant of noise covariance matrix
  double *spectrum; // background spectrum per frequency
};

