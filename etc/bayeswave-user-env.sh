#!/bin/bash -xe
# ----------------------------------------- #
#                 BAYESWAVE                 #
# ----------------------------------------- #

# source this file to access bayeswave

export BAYESWAVE_PREFIX=INSTALL_DIR

# Identify python version
pymajor=$(python -c 'import sys; print(sys.version_info[0])')
pyminor=$(python -c 'import sys; print(sys.version_info[1])')

# BayesWave
export PATH=${BAYESWAVE_PREFIX}/bin:${PATH}
export LD_LIBRARY_PATH=${BAYESWAVE_PREFIX}/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BAYESWAVE_PREFIX}/lib64:${LD_LIBRARY_PATH}
export PYTHONPATH=${BAYESWAVE_PREFIX}/lib/python${pymajor}.${pyminor}/site-packages:${PYTHONPATH}

