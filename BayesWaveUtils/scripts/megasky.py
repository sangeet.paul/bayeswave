#!/usr/bin/env python

from __future__ import print_function

import lal
import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
from lal import GreenwichSiderealTime
import healpy as hp
from ligo.skymap import kde, plot, bayestar, postprocess, io
from astropy.coordinates import SkyCoord
from astropy.time import Time
import getopt
import math
from optparse import OptionParser

from ligo.lw import ligolw
from ligo.lw import lsctables
from ligo.lw import table
from ligo.lw import utils

class LIGOLWContentHandler(ligolw.LIGOLWContentHandler):
    pass


lsctables.use_in(LIGOLWContentHandler)


def parser():
    """
    Parser for input (command line and ini file)
    """

    # --- cmd line
    parser = OptionParser()
    parser.add_option("-M", "--mdc", type=str, default=None)
    parser.add_option("-N", "--nside", type=int, default=128)
    parser.add_option("-I", "--inj", type=str, default=None, help='Injection xml')
    parser.add_option("-e", "--eventnum", type=int, default=None, help='Injection event id')
    parser.add_option("-r", "--ra", type=float, default=None, help="Right Ascension of injection")
    parser.add_option("-d", "--dec", type=float, default=None, help="Declination of injection")
    parser.add_option("-n", "--npost", type=int, default=5000, help="Number of points to use in making skymap")
    parser.add_option("--geo", default=False, action="store_true", help="Whether to project onto world map")
    
    (opts,args) = parser.parse_args()

    if len(args)==0:
        print("ERROR: require BW directory", file=sys.stderr)
        sys.exit()
    if not os.path.isdir(args[0]):
        print("ERROR: BW dir %s does not exist"%args[0], file=sys.stderr)
        sys.exit()


    return opts, args

def gps_to_mjd(gps_time):
    """
    Convert a floating-point GPS time in seconds to a modified Julian day.

    Parameters
    ----------

    gps_time : float
        Time in seconds since GPS epoch

    Returns
    -------

    mjd : float
        Modified Julian day

    Example
    -------

    >>> '%.9f' % round(gps_to_mjd(1129501781.2), 9)
    '57316.937085648'
    """
    gps_seconds_fraction, gps_seconds = math.modf(gps_time)
    mjd = lal.ConvertCivilTimeToMJD(lal.GPSToUTC(int(gps_seconds)))
    return mjd + gps_seconds_fraction / 86400.





def gps_to_iso8601(gps_time):
    """
    Convert a floating-point GPS time in seconds to an ISO 8601 date string.

    Parameters
    ----------

    gps : float
        Time in seconds since GPS epoch

    Returns
    -------

    iso8601 : str
        ISO 8601 date string (with fractional seconds)

    Example
    -------

    >>> gps_to_iso8601(1000000000.01)
    '2011-09-14T01:46:25.010000'
    >>> gps_to_iso8601(1000000000)
    '2011-09-14T01:46:25.000000'
    >>> gps_to_iso8601(1000000000.999999)
    '2011-09-14T01:46:25.999999'
    >>> gps_to_iso8601(1000000000.9999999)
    '2011-09-14T01:46:26.000000'
    >>> gps_to_iso8601(1000000814.999999)
    '2011-09-14T01:59:59.999999'
    >>> gps_to_iso8601(1000000814.9999999)
    '2011-09-14T02:00:00.000000'
    """
    gps_seconds_fraction, gps_seconds = math.modf(gps_time)
    gps_seconds_fraction = '{0:6f}'.format(gps_seconds_fraction)
    if gps_seconds_fraction[0] == '1':
        gps_seconds += 1
    else:
        assert gps_seconds_fraction[0] == '0'
    assert gps_seconds_fraction[1] == '.'
    gps_seconds_fraction = gps_seconds_fraction[1:]
    year, month, day, hour, minute, second, _, _, _ = lal.GPSToUTC(int(gps_seconds))
    return '{0:04d}-{1:02d}-{2:02d}T{3:02d}:{4:02d}:{5:02d}{6:s}'.format(year, month, day, hour, minute, second, gps_seconds_fraction)





# -------------------------------------------
# Module to make skymaps, skyview webpage, etc.
# Works with single output directory from 
# BayesWaveBurst
# -------------------------------------------
def make_skyview(directory='.', mdc=None, NSIDE=128, inj=None, npost=5000, geo=False):

    # -- Close any open figures
    plt.close('all')
    # ----------------
    # Read S6 MDC log file
    # -----------------
    if mdc is None:
        print("No MDC log provided.")

    # -- Save PWD for future reference
    topdir = os.getcwd()

    # -- Enter requested directory
    os.chdir(directory)
    print("Entered", os.getcwd())

    try:
        jobname = 'bayeswave.run'
        bayeswave = open(jobname,'r')
    except:
        sys.exit("\n *bayeswave.run not found! \n")
    
    # ---------------------------------------
    # Extract information from bayeswave.run
    # Note: We may only need the trigger time from this
    # ---------------------------------------
    ifoNames = []
    internal_ra_dec = None
    lines = bayeswave.readlines()
    for line in lines:
        spl = line.split()
        if len(spl) < 1: continue
        
        # Determine names of IFOs and MDC scale factor
        if spl[0] == 'Command' and spl[1] == 'line:':
            for index, splElement in enumerate(spl):
                if splElement == '--ifo':
                    ifoNames.append(spl[index+1]) 
                    
        del spl[-1]
        # Determine number of IFOs
        if ' '.join(spl) == 'number of detectors':
            spl = line.split()
            ifoNum = spl[3]
            continue         
        # Determine gps trigger time
        if ' '.join(spl) == 'trigger time (s)':
            spl = line.split()
            gps = spl[3]
            break
    try:
      injline  = lines[26].split(' ')
      if injline[2]=='Signal':
        print("Found Signal Injection data!")
        internal_ra = float(lines[27].split()[1])
        internal_dec = np.arcsin(float(lines[27].split()[2]))
        internal_ra_dec = [internal_ra, internal_dec]
    except Exception as e:
      print("No internal injection found on line 27 of bayeswave.run")
    bayeswave.close()

    # --------------------------------
    # Create skymap summary statistics
    # --------------------------------

    # -- Input skymap data
    print("Extracting RA/DEC samples")
    try:
       filename = './chains/' + 'signal_params_h0.dat.0'
       data = np.loadtxt(filename, unpack=True,usecols=(0,1,2))
    except:
       filename = './chains/' + 'full_params_h0.dat.0'
       data = np.loadtxt(filename, unpack=True,usecols=(0,1,2))
    ralist = data[1]
    sin_dec = data[2]
    print("Total samples are {0}".format(ralist.size))

    # -- Remove burn in samples
    burnin = int(ralist.size/4)
    ralist = ralist[burnin:]
    sin_dec = sin_dec[burnin:]
    print("After removing burn-in samples are {0}".format(ralist.size))

    declist = np.arcsin(sin_dec)
    thetalist = np.pi/2.0 - declist

    radec = np.column_stack((ralist, declist))

    if radec.shape[0] > npost:
        radec = np.random.permutation(radec)[:npost,:]

    skypost = kde.Clustered2DSkyKDE(radec)

    # -- Get the sidereal time
    print("Finding sidereal time")
    print("Using GPS {0}".format(gps))
    trigtime = float(gps)
    lalgps = lal.LIGOTimeGPS(trigtime)
    sidtime = GreenwichSiderealTime(lalgps, 0)
    sidtime = sidtime % (np.pi*2)

    # -- Get the injection location
    print("GOT MDC?")
    print(mdc)
    if mdc is None:
        injtheta = 0
        injphi   = 0
        injdec   = 0
        injra    = 0
    else:
        injtheta, injphi = mdc.get_theta_phi(trigtime)
        injra = injphi + sidtime
        injdec = np.pi/2 - injtheta
        print("GOT INJECTION PARAMTERS")
        print(injtheta, injra)
   
    if internal_ra_dec is not None:
        injra = internal_ra_dec[0]
        injdec = internal_ra_dec[1]
        print("Got Internal Injection parameters") 

    # -- Make plots directory, if needed
    plotsDir = './plots'
    if not os.path.exists(plotsDir):
        os.makedirs(plotsDir)

 
    # -- Plot the skymap and injection location
    skymap = skypost.as_healpix()
    rhmap = bayestar.rasterize(skymap)
    nside = hp.npix2nside(len(rhmap))
    deg2perpix = hp.nside2pixarea(nside, degrees=True)
    probperdeg2 = rhmap['PROB']/deg2perpix
    vmax = probperdeg2.max()

   
    fig = plt.figure(figsize=(8,6), frameon=False)
    if(geo):
      obstime = Time(trigtime, format='gps').utc.isot
      ax = plt.subplot(111, projection='geo degrees mollweide', obstime=obstime)
    else:
      ax = plt.subplot(111, projection='astro hours mollweide')
    ax.grid()
    ax.imshow_hpx((probperdeg2, 'ICRS'), nested=True, vmin=0., vmax=vmax, cmap='cylon')
    cls = 100 * postprocess.find_greedy_credible_levels(rhmap['PROB'].data)
    cs = ax.contour_hpx((cls, 'ICRS'), nested=True,
            colors='k', linewidths=0.5, levels=[50,90])
    fmt = r'%g\%%' if matplotlib.rcParams['text.usetex'] else '%g%%'
    plt.clabel(cs, fmt=fmt, fontsize=6, inline=True)

    if(geo):
     import json
     geojson_filename = os.path.join(
          os.path.dirname(plot.__file__), 'ne_simplified_coastline.json')
     with open(geojson_filename, 'r') as geojson_file:
        geoms = json.load(geojson_file)['geometries']
     verts = [coord for geom in geoms
               for coord in zip(*geom['coordinates'])]
     plt.plot(*verts, color='0.5', linewidth=0.5,
                 transform=ax.get_transform('world'))


    if (inj is not None):
        ax.plot_coord(SkyCoord(np.rad2deg(inj['ra']), np.rad2deg(inj['dec']), unit='deg'), '*',markerfacecolor='white', markeredgecolor='black', markersize=10)
    if mdc is not None:
        ax.plot_coord(SkyCoord(np.rad2deg(injra), np.rad2deg(injdec), unit='deg'), '*', markerfacecolor='white', markeredgecolor='black', markersize=10)
    if internal_ra_dec is not None:
        ax.plot_coord(SkyCoord(np.rad2deg(injra), np.rad2deg(injdec), unit='deg'), '*', markerfacecolor='white', markeredgecolor='black', markersize=10)
    plt.savefig(plotsDir+'/skymap.png')
    plt.close()
    
    rhmap.meta['origin'] = 'LIGO/Virgo'
    rhmap.meta['gps_creation_time'] = Time.now().gps
    rhmap.meta['gps_time'] = trigtime

    io.write_sky_map('skymap_{0}.fits'.format(gps), rhmap, nest=True)

    # -- Get the found contour and searched areas
    if mdc is not None:
        print("Calculating p value of injection")
        true_ra = injra
        true_dec = injdec
    
    elif (inj is not None):
        print("Calculating p value of command line injection")
        true_ra = inj['ra']
        true_dec = inj['dec']
    elif (internal_ra_dec is not None):
        print("Calculating p value of internal injection")
        true_ra = injra
        true_dec = injdec
    else:
        true_ra = None
        true_dec = None
    (  searched_area, searched_prob, offset, searched_modes, contour_areas, 
       area_probs, contour_modes, searched_prob_dist, contour_dists, 
       searched_vol, searched_prob_vol, contour_vols 
    ) = postprocess.find_injection_moc(skymap,true_ra=true_ra, true_dec=true_dec, contours=[0.5, 0.9])


    outfile = open('skystats.txt', 'w')
    outfile.write("# area50  area90  searcharea  p_value \n")
    outfile.write("{0} {1} {2} {3}".format(contour_areas[0], contour_areas[1], searched_area, searched_prob))
    outfile.close()

    

# -- Write main script for command line running
if __name__ == "__main__":

    # Example command line function calls
    # megasky.py /path/to/working/dir --mdc=/path/to/mdc/mdclog.txt
    # megasky.py --mdc=/path/to/mdc/mdclog.txt

    # Allow navigation into specified working directory
    topdir=os.getcwd()

    # Working directory is current directory unless it is specified by the 
    # first argument of the function call
    opts, args = parser()
         
    ra = opts.ra
    dec = opts.dec
    geo = opts.geo
    mdc = opts.mdc
    NSIDE = opts.nside
    npost = opts.npost

    injpos = None
    # Checl if injection file is present
    if opts.inj is not None:
      # If present, check if event id is provided
      # If yes, proceed. If no, throw error message and exit
      if(opts.eventnum is None):
        print("Provide event num if giving injfile", file=sys.stderr)
        sys.exit()
      print("Loading xml")    
      xmldoc = utils.load_filename(
           opts.inj, contenthandler=LIGOLWContentHandler)
      try:
        print('Checking if using a sim_inspiral table...')
        injs = table.get_table(
            xmldoc, lsctables.SimInspiralTable.tableName)
        inj = injs[opts.eventnum]
        injpos = {
            'ra': inj.longitude,
            'dec': inj.latitude,
            'id': inj.simulation_id}
        print(' yes')
      except:
        print('Checking if using a sim_burst table...')
        injs = table.get_table(xmldoc, lsctables.SimBurstTable.tableName)
        inj = injs[opts.eventnum]
        injpos = {'ra': inj.ra, 'dec': inj.dec, 'id': inj.simulation_id}
        print(' yes')
    
    # If ra and dec provided manually, use those
    if ra is not None and dec is not None:
      injpos  ={'ra': ra, 'dec':dec, 'id':0}
    # If md log provided, use that. Currently,
    # there is no scheme to assert exclusivity of injection and mdcs
    if mdc is not None:
      print('Reading mdc log {0}'.format(mdc))
      try:
        mdc = ft.Mdc(arg)
      except:
        print('Warning! Failed to read mdc')
        mdc = None

    make_skyview(args[0], mdc, NSIDE, injpos, npost, geo)
    # Move back to original dir
    os.chdir(topdir)


